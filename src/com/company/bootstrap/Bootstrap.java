package com.company.bootstrap;

import com.company.table.MyHashTable;

import java.util.HashMap;
import java.util.Map;

public class Bootstrap {

    MyHashTable<Integer, String> myHashTable;

    public void run() {
        myHashTable = new MyHashTable<>();
        myHashTable.add(1, "qrg");
        myHashTable.add(21, "qq");
        myHashTable.add(31, "aafa");
        myHashTable.add(4, "sg");
        myHashTable.add(51, "1414");
        myHashTable.add(11, "FFF");
        myHashTable.add(1, "qq66666");
        print("Изначальный массив");
        print(myHashTable.toString());
        myHashTable.remove(11);
        print("\nМассив после удаления элемента");
        print(myHashTable.toString());
        print("\nСуществует ли элемент в таблице");
        print(String.valueOf(myHashTable.find("sg")));

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "qwe");
        map.put(2, "qw3e");
        map.put(3, "q1we");
        myHashTable = new MyHashTable<>(map);
        print("\nДобавление элементов через Map");
        print(myHashTable.toString());
    }

    private void print(String message) {
        System.out.println(message);
    }
}
