package com.company.table;

import java.util.Arrays;
import java.util.Map;

public class MyHashTable<K, V> {

    private int count_element;
    private static final float REHASH_FACTOR = 0.7f;
    private static final int DEFAULT_SIZE = 11;

    private int thresholdForRehash;
    private Node[] arrayNode;

    private MyHashTable(int size_table) {
        if (size_table <= 0)
            throw new IllegalArgumentException("Неверное значение размера таблицы: " + size_table);

        arrayNode = new Node<?, ?>[size_table];
        thresholdForRehash = (int) (size_table * REHASH_FACTOR);
    }

    public MyHashTable(Map<K, V> map) {
        this(map.size());
        addFull(map);
    }

    public MyHashTable() {
        this(DEFAULT_SIZE);
    }

    private void addFull(Map<K, V> map) {
        for (var element : map.entrySet()) {
            add(element.getKey(), element.getValue());
        }
    }

    public void add(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException();
        }
        Node<?, ?> tab[] = arrayNode;
        if (count_element >= thresholdForRehash) {
            resize();
            tab = arrayNode;
        }
        int hash = key.hashCode();
        int index = hash % arrayNode.length;
        if (!isOldPair(hash, key)) {
            tab[index] = new Node<>(hash, key, value);
            count_element++;
        }
    }

    private void resize() {
        Node<?, ?>[] oldArray = arrayNode;
        arrayNode = Arrays.copyOf(oldArray, arrayNode.length + 10);
        thresholdForRehash = (int) (arrayNode.length * REHASH_FACTOR);
    }

    private boolean isOldPair(int hash, K key) {
        Node<?, ?> tab[] = arrayNode;
        for (int i = 0; i < tab.length; i++) {
            Node<?, ?> e = tab[i];
            if (e != null && (e.getHashcode() == hash) && e.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public boolean find(V value) {
        if (value == null) {
            throw new NullPointerException();
        }
        Node<?, ?> tab[] = arrayNode;
        for (int i = 0; i < tab.length; i++) {
            Node<?, ?> e = tab[i];
            if (e != null && e.getValue().equals(value)) {
                return true;
            }
        }
        return false;
    }

    public void remove(K key) {
        Node<?, ?> tab[] = arrayNode;
        int hash = key.hashCode();
        int index = hash % tab.length;
        for (int i = 0; i < tab.length; i++) {
            Node<?, ?> e = tab[i];
            if (e != null && (e.getHashcode() == hash) && e.getKey().equals(key)) {
                tab[i] = null;
            }
        }
    }

    public String toString() {
        Node<?, ?> tab[] = arrayNode;
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        for (int i = 0; i < tab.length; i++) {
            Node<?, ?> e = tab[i];
            if (e != null) {
                sb.append("KEY:").append(e.getKey());
                sb.append(",");
                sb.append("VALUE:").append(e.getValue());
                sb.append("; ");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private static class Node<K, V> {
        final int hashcode;
        final K key;
        V value;

        public Node(int hashcode, K key, V value) {
            this.hashcode = hashcode;
            this.key = key;
            this.value = value;
        }

        public V getValue() {
            return value;
        }

        public int getHashcode() {
            return hashcode;
        }

        public K getKey() {
            return key;
        }
    }


}
